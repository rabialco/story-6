# Story 6
This Gitlab repository is the result of the work from **Rabialco Argana**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/rabialco/story-6/badges/master/pipeline.svg)](https://gitlab.com/rabialco/story-6/commits/master)
[![coverage report](https://gitlab.com/rabialco/story-6/badges/master/coverage.svg)](https://gitlab.com/rabialco/story-6/commits/master)

## URL
This lab projects can be accessed from [https://rabialco-story6.herokuapp.com](https://rabialco-story6.herokuapp.com)

## Author
**Rabialco Argana** - [rabialco](https://gitlab.com/rabialco)
