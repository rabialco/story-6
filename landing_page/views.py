from django.shortcuts import render, redirect
from .forms import *
from .models import input

# Create your views here.
# def home(request):
#     return render(request, 'index.html')

def home(request):
    if request.method == "POST":
        form = status_input(request.POST)
        if (form.is_valid()):
            status = input()
            status.status_user = form.cleaned_data['status_user']
            status.save()
        return redirect('/')
    else:
        form = status_input()
        status = input.objects.all().order_by('-waktu_user')
        response = {'status' : status, 'form': form}
        return render(request, 'index.html', response)
