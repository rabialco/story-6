from django import forms
from .models import input

class status_input(forms.Form):
    # class Meta:
    #     model = input
    #     fields = ['status_user']
    #     widgets = {
    #         'status_user' : forms.CharField(attrs={'placeholder' : 'Masukkan status anda','style' : 'border:0; width:100%'})
    #     }
    status_user = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Masukkan status anda',
        'type' : 'text',
        'maxlength' : '300',
        'required' : True,
        'style' : 'border:0; width:100%;text-align:center;background:black;color:white;',
        'label' : '',
    }))